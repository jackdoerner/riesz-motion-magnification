import numpy, cv2, scipy.signal
from boundary import *

lowpass = numpy.asarray([
	[-0.0001,   -0.0007,  -0.0023,  -0.0046,  -0.0057,  -0.0046,  -0.0023,  -0.0007,  -0.0001],
	[-0.0007,   -0.0030,  -0.0047,  -0.0025,  -0.0003,  -0.0025,  -0.0047,  -0.0030,  -0.0007],
	[-0.0023,   -0.0047,   0.0054,   0.0272,   0.0387,   0.0272,   0.0054,  -0.0047,  -0.0023],
	[-0.0046,   -0.0025,   0.0272,   0.0706,   0.0910,   0.0706,   0.0272,  -0.0025,  -0.0046],
	[-0.0057,   -0.0003,   0.0387,   0.0910,   0.1138,   0.0910,   0.0387,  -0.0003,  -0.0057],
	[-0.0046,   -0.0025,   0.0272,   0.0706,   0.0910,   0.0706,   0.0272,  -0.0025,  -0.0046],
	[-0.0023,   -0.0047,   0.0054,   0.0272,   0.0387,   0.0272,   0.0054,  -0.0047,  -0.0023],
	[-0.0007,   -0.0030,  -0.0047,  -0.0025,  -0.0003,  -0.0025,  -0.0047,  -0.0030,  -0.0007],
	[-0.0001,   -0.0007,  -0.0023,  -0.0046,  -0.0057,  -0.0046,  -0.0023,  -0.0007,  -0.0001]
])

highpass = numpy.asarray([
	[0.0000,   0.0003,   0.0011,   0.0022,   0.0027,   0.0022,   0.0011,   0.0003,   0.0000],
	[0.0003,   0.0020,   0.0059,   0.0103,   0.0123,   0.0103,   0.0059,   0.0020,   0.0003],
	[0.0011,   0.0059,   0.0151,   0.0249,   0.0292,   0.0249,   0.0151,   0.0059,   0.0011],
	[0.0022,   0.0103,   0.0249,   0.0402,   0.0469,   0.0402,   0.0249,   0.0103,   0.0022],
	[0.0027,   0.0123,   0.0292,   0.0469,  -0.9455,   0.0469,   0.0292,   0.0123,   0.0027],
	[0.0022,   0.0103,   0.0249,   0.0402,   0.0469,   0.0402,   0.0249,   0.0103,   0.0022],
	[0.0011,   0.0059,   0.0151,   0.0249,   0.0292,   0.0249,   0.0151,   0.0059,   0.0011],
	[0.0003,   0.0020,   0.0059,   0.0103,   0.0123,   0.0103,   0.0059,   0.0020,   0.0003],
	[0.0000,   0.0003,   0.0011,   0.0022,   0.0027,   0.0022,   0.0011,   0.0003,   0.0000]
])

def getsize(img):
	h, w = img.shape[:2]
	return w, h

def build_laplacian(img, minsize=2, convolutionThreshold=500, dtype=numpy.float64):
	img = dtype(img)
	levels = []
	while (min(img.shape) > minsize):

		if (img.size < convolutionThreshold):
			convolutionFunction = scipy.signal.convolve2d
		else:
			convolutionFunction = scipy.signal.fftconvolve

		w, h = getsize(img)
		symmimg = symmetrical_boundary(img)
		
		hp_img = convolutionFunction(symmimg, highpass, mode='same')[h/2:-h/2,w/2:-w/2]
		lp_img = convolutionFunction(symmimg, lowpass, mode='same')[h/2:-h/2,w/2:-w/2]
		levels.append(hp_img)

		img = cv2.resize(lp_img, ((w+1)/2, (h+1)/2))

	levels.append(img)
	return levels

def collapse_laplacian(levels, convolutionThreshold=500):
	img = levels[-1]
	for lev_img in levels[-2::-1]:

		img = cv2.resize(img, getsize(lev_img))

		if (img.size < convolutionThreshold):
			convolutionFunction = scipy.signal.convolve2d
		else:
			convolutionFunction = scipy.signal.fftconvolve

		w, h = getsize(img)
		symmimg = symmetrical_boundary(img)
		symmlev = symmetrical_boundary(lev_img)

		img = convolutionFunction(symmimg, lowpass, mode='same')[h/2:-h/2,w/2:-w/2]
		img += convolutionFunction(symmlev, highpass, mode='same')[h/2:-h/2,w/2:-w/2]
	return img