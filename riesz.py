import numpy, math
import scipy, scipy.signal

#riesz_band_filter = numpy.asarray([[-0.5, 0, 0.5]])
#riesz_band_filter = numpy.asarray([[-0.2,-0.48, 0, 0.48,0.2]])
riesz_band_filter = numpy.asarray([[-0.12,0,0.12],[-0.34, 0, 0.34],[-0.12,0,0.12]])

def laplacian_to_riesz(pyr):
	newpyr = [pyr[0], [], []]
	for ii in range(len(pyr[0])):
		newpyr[1].append( scipy.signal.convolve2d(pyr[0][ii], riesz_band_filter, mode='same', boundary='symm') )
		newpyr[2].append( scipy.signal.convolve2d(pyr[0][ii], riesz_band_filter.T, mode='same', boundary='symm') )
	return newpyr

def riesz_to_spherical(pyr):
	newpyr = [pyr[0], [], [], [], [], pyr[1], pyr[2]]
	for ii in range(len(pyr[0]) ):
		I = pyr[0][ii]
		R1 = pyr[1][ii]
		R2 = pyr[2][ii]
		A = numpy.sqrt(I*I + R1*R1 + R2*R2)
		theta = numpy.arctan2(R2,R1)
		Q = R1 * numpy.cos(theta) + R2 * numpy.sin(theta)
		phi = numpy.arctan2(Q,I)

		newpyr[1].append( A )
		newpyr[2].append( theta )
		newpyr[3].append( phi )
		newpyr[4].append( Q )
	return newpyr


def riesz_spherical_to_laplacian(pyr):
	newpyr = [[]]
	for ii in range(len(pyr[0])):
		newpyr[0].append( pyr[1][ii] * numpy.cos( pyr[3][ii] ) )
	return newpyr

