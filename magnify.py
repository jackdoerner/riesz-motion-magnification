input_file = 'input.mp4'
output_file = 'amplified.mp4'

passwindow = 96
framerate = 24
scaling_factor = 30
max_frames = -1
filter_type = 'bandpass'
detrending = None
order = 0

bandpassmin = 2.0
bandpassmax = 3.0

convolutionThreshold = 5000

import sys, math, copy

import numpy, cv2
import scipy, scipy.signal, scipy.integrate

from fftconvolve_tools import get_fshape, trim_convolved, convolve_from_fft, auto_fft
from laplacian_like import *
from riesz import *

cap = cv2.VideoCapture(input_file)

#fourcc = cv2.cv.CV_FOURCC('A', 'V', 'C', '1')   
save = cv2.VideoWriter(
    filename=output_file,
    fourcc=int(cap.get(cv2.cv.CV_CAP_PROP_FOURCC)),
    fps=cap.get(cv2.cv.CV_CAP_PROP_FPS),
    frameSize=(int(cap.get(cv2.cv.CV_CAP_PROP_FRAME_WIDTH)),int(cap.get(cv2.cv.CV_CAP_PROP_FRAME_HEIGHT))),
    )

diffs = cv2.VideoWriter(
    filename='diffs.mp4',
    fourcc=int(cap.get(cv2.cv.CV_CAP_PROP_FOURCC)),
    fps=cap.get(cv2.cv.CV_CAP_PROP_FPS),
    frameSize=(int(cap.get(cv2.cv.CV_CAP_PROP_FRAME_WIDTH)),int(cap.get(cv2.cv.CV_CAP_PROP_FRAME_HEIGHT))),
    )


#framerate = cap.get(cv2.cv.CV_CAP_PROP_FPS)
ret, frame = cap.read()

origvideo = []
windowdata = []
framecount = 0

gaussiankernel = cv2.getGaussianKernel(10,-1)

if (filter_type == 'bandpass'):
	(filt_b, filt_a) = scipy.signal.butter(2, (bandpassmin/(framerate/2), bandpassmax/(framerate/2)), btype='bandpass', output='ba')
else:
	(filt_b, filt_a) = scipy.signal.butter(2, bandpassmax/(framerate/2), btype='lowpass', output='ba')

print
print 'processing frame     ',

while(ret and framecount != max_frames):
	framecount += 1
	print '\b\b\b\b\b{:04d}'.format(framecount),
	sys.stdout.flush()

	gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
	origvideo.append(gray)
	pyr = [build_laplacian(gray, minsize=10, convolutionThreshold=convolutionThreshold)]
	rieszpyr = laplacian_to_riesz(pyr)
	polar_pyr = riesz_to_spherical(rieszpyr)
	for jj in range(0, max(0, len(origvideo) - passwindow)):
		origvideo.pop(0)

	for ii in range(len(polar_pyr[3])):
		if (ii >= len(windowdata)):
			windowdata.append(numpy.empty((polar_pyr[3][ii].shape[0],polar_pyr[3][ii].shape[1],passwindow)))
		windowdata[ii] = numpy.roll(windowdata[ii], 1, axis=2)
		windowdata[ii][:,:,0] = polar_pyr[3][ii]
	
	if (len(origvideo) > passwindow/2):
		frameindex = -passwindow/2
		currentgray = origvideo[frameindex]
		pyr = [build_laplacian(currentgray, minsize=10, convolutionThreshold=convolutionThreshold)]
		rieszpyr = laplacian_to_riesz(pyr)
		currentframe = riesz_to_spherical(rieszpyr)
		
		for jj in range(len(windowdata)):

			data = windowdata[jj][:,:,:len(origvideo)].copy()
			if (order > 0):
				data = numpy.diff(data, n=order, axis=2)
			if (not (detrending is None or detrending is 'none')):
				data = scipy.signal.detrend(data,axis=2,type=detrending)
			data = scipy.signal.filtfilt(filt_b, filt_a, data, axis=2)
			for ii in range(0,order):
				data = scipy.integrate.cumtrapz(data, axis=2)
			signal = data[:,:,passwindow + frameindex -len(origvideo) + order]
			denom = scipy.signal.convolve(scipy.signal.convolve(currentframe[1][jj], gaussiankernel.T, mode='same'), gaussiankernel, mode='same')
			denom_zero = (denom == 0)
			denom = numpy.where(denom_zero, 1, denom)
			signal = numpy.where(denom_zero, 0, scipy.signal.convolve(scipy.signal.convolve(currentframe[1][jj] * signal, gaussiankernel.T, mode='same'), gaussiankernel, mode='same') / denom)
			currentframe[3][jj] += numpy.real(signal) * (scaling_factor-1)

		derieszpyr = riesz_spherical_to_laplacian(currentframe)
		recon = collapse_laplacian(derieszpyr[0])
		recon = numpy.select([recon > 255, recon < 0, True], [255, 0, recon])
		recon = (recon).astype(numpy.uint8)

		save.write(cv2.cvtColor(recon,cv2.COLOR_GRAY2RGB))

		diff = numpy.abs(recon - currentgray)
		diffs.write(cv2.cvtColor(diff,cv2.COLOR_GRAY2RGB))


	ret, frame = cap.read()


origvideo.pop(0)
for ii in range(len(windowdata)):
	windowdata[ii] = numpy.roll(windowdata[ii], 1, axis=2)
while (len(origvideo) >= passwindow/2):
	frameindex = -passwindow/2
	currentframe = []

	currentgray = origvideo[frameindex]
	pyr = [build_laplacian(currentgray, minsize=10, convolutionThreshold=convolutionThreshold)]
	rieszpyr = laplacian_to_riesz(pyr)
	currentframe = riesz_to_spherical(rieszpyr)
	
	for jj in range(len(windowdata[3])):

		data = windowdata[jj][:,:,:len(origvideo)].copy()
		if (order > 0):
			data = numpy.diff(data, n=order, axis=2)
		if (not (detrending is None or detrending is 'none')):
			data = scipy.signal.detrend(data,axis=2,type=detrending)
		data = scipy.signal.filtfilt(filt_b, filt_a, data, axis=2)
		for ii in range(0,order):
			data = scipy.integrate.cumtrapz(data, axis=2)
		signal = data[:,:,frameindex + order]
		denom = scipy.signal.convolve(scipy.signal.convolve(currentframe[1][jj], gaussiankernel.T, mode='same'), gaussiankernel, mode='same')
		denom_zero = (denom == 0)
		denom = numpy.where(denom_zero, 1, denom)
		signal = numpy.where(denom_zero, 0, scipy.signal.convolve(scipy.signal.convolve(currentframe[1][jj] * signal, gaussiankernel.T, mode='same'), gaussiankernel, mode='same') / denom)
		currentframe[3][jj] += numpy.real(signal) * (scaling_factor-1)

	derieszpyr = riesz_spherical_to_laplacian(currentframe)
	recon = collapse_laplacian(derieszpyr[0])
	recon = numpy.select([recon > 255, recon < 0, True], [255, 0, recon])
	recon = (recon).astype(numpy.uint8)

	save.write(cv2.cvtColor(recon,cv2.COLOR_GRAY2RGB))

	currentgray = origvideo[frameindex]
	diff = numpy.abs(recon - currentgray)
	diffs.write(cv2.cvtColor(diff,cv2.COLOR_GRAY2RGB))

	origvideo.pop(0)
	for ii in range(len(windowdata)):
		windowdata[ii] = numpy.roll(windowdata[ii], 1, axis=2)


cap.release()